import { createRouter, createWebHistory } from 'vue-router';
import AllFood from './Pages/Menu/AllFood.vue';
import AddCart from './Pages/Cart/AddCart.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/AllFood' },
    { path: '/AllFood', component: AllFood },
    { path: '/Cart', component: AddCart },
  ],
});

export default router;
